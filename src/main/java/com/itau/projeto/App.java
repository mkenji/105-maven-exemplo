package com.itau.projeto;


import java.util.ArrayList;


import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	Gson gson = new Gson();
    	String endPoint = "http://data.fixer.io/api/latest?";
    	String accessCode = "access_key=adca3badaa8fb6889ae0a7c2a66102a1";
    	String response = HttpRequest.get(endPoint+accessCode).body();
    	
    	Fixer fixer = gson.fromJson(response, Fixer.class);
    	
    	System.out.println(fixer.rates.get("BDT"));
    }
}
